# FossiumBot
Welcome to the GitHub page of FossiumBot!
The bot is written in C# using .NET and [DSharpPlus](https://dsharpplus.github.io).

## Features
The bot has a lot of features, including but not limited to:
- Moderation
  - Warning system
  - Mute system
  - Kick, ban, softban
  - Purge
  - Autodelete
- Utils
  - Avatar, userinfo, serverinfo
- Fun
  - Dog, cat
  - GitHub
  - Wikipedia

## Installing
Installation is pretty easy, just download the [latest release](https://github.com/Fossium-Team/FossiumBot/releases/latest), unzip, and run the excecutable.\
If you can't use the commands right away, don't worry, slash commands can take some time to register and update.

## Support Server
[![Join the support server](https://discord.com/api/guilds/848464241219338250/widget.png?style=banner2)](http://discord.gg/myzbqnVUFN)
